#! /bin/bash
# set path to site directory, relative to location of script
echo "Installing Buyography"

CMDS="virtualenv clang g++ pip"

for i in $CMDS
do
    type -P $i &>/dev/null && echo "Found $i" && continue || echo "you need to install $i before proceeding." && exit 1
done


if [ -d "buyography" ]; then
   echo "buyography directory already exists."
   echo "do you want to remove it first? (y or n)"
   read RESP
   if [ "$RESP" = "y" ]; then
     rm -rf buyography
   else
     exit 1
   fi
fi

git clone git@bitbucket.org:damon_c/buyography.git
cd buyography
# cd "`dirname \"$0\"`/.."
virtualenv .
echo "export PYTHONPATH=`pwd`:`pwd`/source/" >> bin/activate
echo "export DJANGO_SETTINGS_MODULE=base.settings.local" >>bin/activate
source bin/activate
cd source
pip install -r requirements.txt
./manage.py syncdb --noinput
./manage.py migrate socialaccount --noinput
./manage.py migrate --noinput
./manage.py syncdb --noinput
./manage.py loaddata apps/buyography/fixtures/site_setup.json
./manage.py loaddata apps/buyography/fixtures/brands.json
./manage.py loaddata apps/buyography/fixtures/acquisitions.json
./manage.py loaddata apps/buyography/fixtures/things.json
./manage.py loaddata apps/buyography/fixtures/thingsources.json
./manage.py loaddata apps/buyography/fixtures/thingtypes.json
echo "All done."

hx start --dev --nocache --http_port 8000 --settings base.settings.local --reload